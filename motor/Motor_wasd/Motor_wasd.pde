//. Motor driver shield- 2012 Copyright (c) Seeed Technology Inc.
// 


int pinI1=8;//define I1 interface
int pinI2=11;//define I2 interface 
int speedpinA=9;//enable motor A
int pinI3=12;//define I3 interface 
int pinI4=13;//define I4 interface 
int speedpinB=10;//enable motor B
int speed = 255;//define the speed of motor

//Uno32 External Interrupt pins:  ** INT0 = 38, INT1 = 2, INT2 = 7, INT3 = 8, INT4 = 35
int ENCODER_L = 2;  // pin #
int ENCODER_R = 7;  // pin #
int encoder0Pos = 0;
int encoder1Pos = 0;


// ENCODERS //
void ENCODER_PULSE_L(){
encoder0Pos++;}

void ENCODER_PUSLE_R(){
encoder1Pos++;}

void setup()
{
  pinMode(pinI1,OUTPUT);
  pinMode(pinI2,OUTPUT);
  pinMode(speedpinA,OUTPUT);
  pinMode(pinI3,OUTPUT);
  pinMode(pinI4,OUTPUT);
  pinMode(speedpinB,OUTPUT);
  digitalWrite(speedpinA,LOW);// Unenble the pin, to stop the motor. this should be done to avid damaging the motor. 
  digitalWrite(speedpinB,LOW);

  pinMode (ENCODER_L,INPUT);
  pinMode (ENCODER_R,INPUT);

  Serial.begin (115200);
  
  Serial.println("You have now 20 sec to type commands: f,b,l,r or digit and to unplug the cable before roomba starts");
  delay(1000);
  delay(20000);  //time to unplug the link with computer
  
  attachInterrupt(1, ENCODER_PULSE_L, RISING); // interrupt #1 is on pin 2, watch left encoder for rising pulse
  attachInterrupt(2, ENCODER_PUSLE_R, RISING); // interrupt #2 is on pin 7, watch right encoder for rising pulse   
}
 
void forward()
{
     analogWrite(speedpinA,speed);//input a simulation value to set the speed
     analogWrite(speedpinB,speed);
     digitalWrite(pinI4,HIGH);//turn DC Motor B move clockwise
     digitalWrite(pinI3,LOW);
     digitalWrite(pinI2,LOW);//turn DC Motor A move anticlockwise
     digitalWrite(pinI1,HIGH);
}
void backward()//
{
     analogWrite(speedpinA,speed);//input a simulation value to set the speed
     analogWrite(speedpinB,speed);
     digitalWrite(pinI4,LOW);//turn DC Motor B move anticlockwise
     digitalWrite(pinI3,HIGH);
     digitalWrite(pinI2,HIGH);//turn DC Motor A move clockwise
     digitalWrite(pinI1,LOW);
}

void right()
{
     analogWrite(speedpinA,speed);//input a simulation value to set the speed
     analogWrite(speedpinB,speed);
     digitalWrite(pinI4,HIGH);//turn DC Motor B move clockwise
     digitalWrite(pinI3,LOW);
     digitalWrite(pinI2,HIGH);//turn DC Motor A move clockwise
     digitalWrite(pinI1,LOW);
}

void left()
{
     analogWrite(speedpinA,speed);//input a simulation value to set the speed
     analogWrite(speedpinB,speed);
     digitalWrite(pinI4,LOW);//turn DC Motor B move anticlockwise
     digitalWrite(pinI3,HIGH);
     digitalWrite(pinI2,LOW);//turn DC Motor A move clockwise
     digitalWrite(pinI1,HIGH);
}

void stop()
{
     digitalWrite(speedpinA,LOW);// Unenble the pin, to stop the motor. this should be done to avid damaging the motor. 
     digitalWrite(speedpinB,LOW);
     delay(100);
 
}

void loop()
{
  char incomingByte, x;
  
  if(Serial.available() <= 0)
    stop(); 
  else
  {
    switch(incomingByte = Serial.read())
    {
      case 'a' :
          left();
          break;
       case 'd' :
          right();
          break;
       case 'w' :
          forward();
          break;
       case 's' :
          backward();
          break;
       case '\r':
       case '\n':
          break;       
       default :
          stop();
          break;
     }
    delay(700);
  }
    
  Serial.print(encoder0Pos);   Serial.print(' ');   Serial.println(encoder1Pos);  
}


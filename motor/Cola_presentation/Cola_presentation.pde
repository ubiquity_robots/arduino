//#define MESSAGE_INFO

#define real float

int const LEFT_PIN_PWM = 5;
int const RIGHT_PIN_PWM = 6;

int const SONIC_FRONT_START_PIN = -1;  //Originally 32
int const SONIC_FRONT_READ_PIN = -1;   //Originally 32
int const SONIC_LEFT_PIN = 7;
int const SONIC_RIGHT_PIN = 8;

static long const SONIC_TRIGGER_DISTANCE_CM = 25;
static long const SONIC_TIMEOUT = 5000;

static real const SPEED_DELTA = 1.0;
static real const ROTATION_DELTA = 10.0;
static long const SPEED_MAX = 30;
static long const SPEED_MIN = 15;
static real const TRANSIT_TIME = 3.0;  //Used to convert distance from computer into speed

long labs(long const i_value)
{
  return i_value < 0 ? -i_value : i_value;
}

real rabs(real const i_value)
{
  return i_value < 0 ? -i_value : i_value;
}

real cap(real const i_value, real const i_min, real const i_max)
{
  if(i_value < i_min)
    return i_min;
  else if(i_value > i_max)
    return i_max;
    
  return i_value;
}

void setup()
{
  Serial.begin(115200);
  
  pinMode(LEFT_PIN_PWM, OUTPUT);
  analogWrite(LEFT_PIN_PWM, 0);
  pinMode(RIGHT_PIN_PWM, OUTPUT);
  analogWrite(RIGHT_PIN_PWM, 0);
  
  Serial.println("Ready.");
}

/*
static long gs_rotationArrayCurrent = 0;
static long const gsc_rotationArrayMax = 10;
static real gs_rotationArray[gsc_rotationArrayMax];
static long gs_speedArraycurrent = 0;
static long const gsc_speedArrayMax = 10;
static real gs_speedArray[gsc_speedArrayMax];
*/
static real gs_desiredRotation = 0;
static real gs_desiredSpeed = 0;
static real gs_currentRotation = 0;
static real gs_currentSpeed = 0;
void loop()
{
  long frontDistance = -1;
  long leftDistance = -1;
  long rightDistance = -1;
  int inputCharacter = Serial.read();
  switch(inputCharacter)
  {
    case 'G':
      char radiusString[4];
      char thetaString[4];
      
      delay(5);
      
      radiusString[0] = ((char)Serial.read());
      radiusString[1] = ((char)Serial.read());
      radiusString[2] = ((char)Serial.read());
      radiusString[3] = '\0';
      thetaString[0] = ((char)Serial.read());
      thetaString[1] = ((char)Serial.read());
      thetaString[2] = ((char)Serial.read());
      thetaString[3] = '\0';

      gs_desiredSpeed=atol(radiusString)/TRANSIT_TIME;
      gs_desiredRotation=atol(thetaString);
      while(gs_desiredRotation > 180.0)
      {
        gs_desiredRotation -= 360.0;
      }
      break;
    case ' ':
      gs_desiredSpeed = 0.0;
      gs_desiredRotation = 0.0;
      break;
    default:
      break;
  }
  
  if(SONIC_FRONT_START_PIN > -1 && SONIC_FRONT_READ_PIN > -1)
  {
    frontDistance = vexSensor(SONIC_FRONT_START_PIN, SONIC_FRONT_READ_PIN);
    if(frontDistance > -1)
    {
      if(frontDistance < SONIC_TRIGGER_DISTANCE_CM && (rabs(gs_desiredRotation) < 90))
      {
        gs_desiredSpeed = 0;
      }
      else if(frontDistance < (SONIC_TRIGGER_DISTANCE_CM/2))
      {
        gs_desiredSpeed = 0;
      }
    }
  }
  
  if(SONIC_LEFT_PIN > -1)
  {
    leftDistance = pingSensor(SONIC_LEFT_PIN);
    if(leftDistance > -1 && leftDistance < SONIC_TRIGGER_DISTANCE_CM)
    {
      if(gs_desiredRotation <= 0)
      {
        gs_desiredRotation = 90;
      }
#ifdef MESSAGE_INFO
      Serial.println("LEFT ULTRASONIC: " + String(leftDistance) + " cm");
#endif
    }
  }
  
  if(SONIC_RIGHT_PIN > -1)
  {
    rightDistance = pingSensor(SONIC_RIGHT_PIN);
    if(rightDistance > -1 && rightDistance < SONIC_TRIGGER_DISTANCE_CM)
    {
      if(gs_desiredRotation >= 0)
      {
        gs_desiredRotation = -90;
      }
      
#ifdef MESSAGE_INFO
      Serial.println("RIGHT ULTRASONIC: " + String(rightDistance) + " cm");
#endif
    }
  }
  
  /***** Emergency Stop *****/
  if((leftDistance > -1 && leftDistance < SONIC_TRIGGER_DISTANCE_CM) && (rightDistance > -1 && rightDistance < SONIC_TRIGGER_DISTANCE_CM))
  {
    gs_desiredSpeed = 0;    
  }
  
  /***** Determine motor amount *****/
  long left=0;
  long right=0;
  
  if(gs_currentSpeed == 0.0)
  {
    gs_currentRotation = gs_desiredRotation;
  }
  else
  {
    gs_currentRotation = gs_currentRotation + cap(gs_desiredRotation - gs_currentRotation, -ROTATION_DELTA, ROTATION_DELTA);
  }
  gs_currentSpeed = gs_currentSpeed + cap(gs_desiredSpeed - gs_currentSpeed, -SPEED_DELTA, SPEED_DELTA);

  real const spot=0.5+gs_currentRotation/180.0;
  left = gs_currentSpeed*spot;
  right = gs_currentSpeed*(1.0-spot);
  /*
  if(gs_currentRotation>0.0)
  {
    left=(gs_currentSpeed);
    right=(gs_currentSpeed)*((90.0-gs_currentRotation)/90.0);
  }
  else if(gs_currentRotation<0.0) {
    left=(gs_currentSpeed)*((90.0+gs_currentRotation)/90.0);
    right=(gs_currentSpeed);
  }
  else {
    left=(gs_currentSpeed);
    right=(gs_currentSpeed);
  }*/
  
#ifdef MESSAGE_INFO
  Serial.println(String("S: (")+long(gs_desiredSpeed)+String(") R: ")+long(gs_desiredRotation)+String(" L: ")+left+String(" R: ")+right);
#endif

  if(left <= SPEED_MIN)
  {
    left = 0;
  }
  if(right <= SPEED_MIN)
  {
    right = 0;
  }

  analogWrite(LEFT_PIN_PWM, cap(labs(left), 0, SPEED_MAX));
  analogWrite(RIGHT_PIN_PWM, cap(labs(right), 0, SPEED_MAX));
}

long operateUltrasonicSensor(int const i_startPulsePin, int const i_readPulsePin, long const startPulseDelay, long const endPulseDelay)
{
  /***** Send pulse *****/
  pinMode(i_startPulsePin, OUTPUT);
  digitalWrite(i_startPulsePin, LOW);
  delayMicroseconds(startPulseDelay);
  digitalWrite(i_startPulsePin, HIGH);
  delayMicroseconds(endPulseDelay);
  digitalWrite(i_startPulsePin, LOW);
  
  /***** Receive pulse *****/
  pinMode(i_readPulsePin, INPUT);
  long const duration = pulseIn(i_readPulsePin, HIGH, SONIC_TIMEOUT);
  if(duration <= 0)
  {
    return -1;
  }
  
  return duration;
}

long hy_srf05_sensor (int const startPulsePin, int const readPulsePin) // HY- SRF05 Uses two digital pins
{
  /***** Return -1 on timeout *****/
  long const duration = operateUltrasonicSensor(startPulsePin, readPulsePin, 2, 5);
  if(duration <= 0)
  {
    return -1;
  }
  
  long const cm = microsecondsToCentimeters(duration);
  return cm;
}

long vexSensor(int const startPulsePin, int const readPulsePin) // Vex model 276-2155
{
  /***** Return -1 on timeout *****/
  long const duration = operateUltrasonicSensor(startPulsePin, readPulsePin, 2, 5);
  if(duration <= 0)
  {
    return -1;
  }
  
  /***** Return -1 on error *****/
  long const cm = microsecondsToCentimeters(duration);
  return cm;
}

long pingSensor(int const pinNumber) // Sensor Model Ping ))) and manufaturer www.parallax.com
{
  /***** Return -1 on timeout *****/
  long const duration = operateUltrasonicSensor(pinNumber, pinNumber, 2, 5);
  if(duration <= 0)
  {
    return -1;
  }
  
  /***** Return -1 on blocked sensor *****/
  long const cm = microsecondsToCentimeters(duration);
  if (cm > 300)
  {
    return -1;
  }
  
  return cm;
}

/*
inline long microsecondsToInches(long const i_microseconds)
{
  // According to Parallax's datasheet for the PING))), there are
  // 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
  // second). This gives the distance travelled by the ping, outbound
  // and return, so we divide by 2 to get the distance of the obstacle.
  // See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
  
  return long(double(i_microseconds) / 74.6422169 / 2.0);
}
*/

inline long microsecondsToCentimeters(long const i_microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return long(double(i_microseconds) / 29.3866996 / 2.0);
}

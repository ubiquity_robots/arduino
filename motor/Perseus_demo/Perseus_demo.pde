#include <plib.h>
//. Motor driver shield- 2012 Copyright (c) Seeed Technology Inc.
// 
//#define MESSAGE_INFO

static int const PIN_I1=8;//define I1 interface
static int const PIN_I2=11;//define I2 interface 
static int const RIGHT_PIN_POWER=9;//enable motor A
static int const PIN_I3=12;//define I3 interface 
static int const PIN_I4=13;//define I4 interface 
static int const LEFT_PIN_POWER=10;//enable motor B
static int const SPEED_CURRENT = 125;
static int const SPEED_MAX = 255;

enum Result
{
  R_TRUE=1,
  R_FALSE=0,
  R_SUCCESS=0,
  R_FAILURE=-1,
  R_INPUTBAD=-2,
  R_NEEDSINIT=-3
};

int cap(int const i_value, int const i_min, int const i_max)
{
  if(i_value < i_min)
    return i_min;
  else if(i_value > i_max)
    return i_max;
    
  return i_value;
}

void servoWrite(uint8_t const i_pin, uint8_t const i_magnitude255)
{
  analogWrite(i_pin, i_magnitude255);
}

class MotorPerseus
{
public:
  MotorPerseus(int const i_leftPinPower, int const i_rightPinPower, int const i_I1, int const i_I2, int const i_I3, int const i_I4);
  ~MotorPerseus();
  Result init();
  Result drive(int const i_speedLeft, int const i_speedRight);
  
  int speedLeft() {
    return m_speedLeftCurrent;
  }
  int speedRight() {
    return m_speedRightCurrent;
  }
  
  private:
    int const mc_I1;
    int const mc_I2;
    int const mc_I3;
    int const mc_I4;
    int const mc_leftPinPower;
    int const mc_rightPinPower;
    
    bool m_setup;
    
    int m_speedLeftCurrent;
    int m_speedLeftDesired;
    int m_speedRightCurrent;
    int m_speedRightDesired;
    
    static int const msc_speedIncrementDelayMillis = 0;
    static int const msc_switchDelayMillis = 10;
};

MotorPerseus::MotorPerseus(int const i_leftPinPower, int const i_rightPinPower, int const i_I1, int const i_I2, int const i_I3, int const i_I4)
:m_setup(false), m_speedLeftCurrent(0), m_speedLeftDesired(0), m_speedRightCurrent(0), m_speedRightDesired(0),
mc_leftPinPower(i_leftPinPower), mc_rightPinPower(i_rightPinPower), mc_I1(i_I1), mc_I2(i_I2), mc_I3(i_I3), mc_I4(i_I4)
{
}

MotorPerseus::~MotorPerseus()
{
  if(m_setup)
  {
    digitalWrite(mc_leftPinPower, LOW);
    digitalWrite(mc_rightPinPower, LOW);
    
    delay(msc_switchDelayMillis);
    digitalWrite(mc_I1, HIGH);
    digitalWrite(mc_I2, LOW);
    digitalWrite(mc_I3, LOW);
    digitalWrite(mc_I4, HIGH);
    delay(msc_switchDelayMillis);
  }
}
  
Result MotorPerseus::init()
{
  pinMode(mc_leftPinPower, OUTPUT);
  servoWrite(mc_leftPinPower, 0);
  pinMode(mc_rightPinPower, OUTPUT);
  servoWrite(mc_rightPinPower, 0);
  
  delay(msc_switchDelayMillis);
  pinMode(mc_I1, OUTPUT);
  digitalWrite(mc_I1,HIGH);
  pinMode(mc_I2, OUTPUT);
  digitalWrite(mc_I2,LOW);
  pinMode(mc_I3, OUTPUT);
  digitalWrite(mc_I3,LOW);
  pinMode(mc_I4, OUTPUT);
  digitalWrite(mc_I4,HIGH);
  delay(msc_switchDelayMillis);
  
  m_setup = true;
  
  return R_SUCCESS;
}

Result MotorPerseus::drive(int const i_speedLeft, int const i_speedRight)
{
  if(!m_setup)
  {
    return R_NEEDSINIT;
  }
  
  bool inputLeftReverse = i_speedLeft < 0;
  bool inputRightReverse = i_speedRight < 0;
  
  m_speedLeftDesired = cap(i_speedLeft, -SPEED_MAX, SPEED_MAX);
  m_speedRightDesired = cap(i_speedRight, -SPEED_MAX, SPEED_MAX);
      
  servoWrite(mc_leftPinPower, cap(abs(m_speedLeftDesired), 0, SPEED_MAX));
  servoWrite(mc_rightPinPower, cap(abs(m_speedRightDesired), 0, SPEED_MAX));
  
  //if(    ((i_speedLeft < 0) != (m_speedLeftCurrent < 0))
  //    || ((i_speedRight < 0) != (m_speedRightCurrent < 0)))
  {
    delay(msc_switchDelayMillis);
    
    if(!inputLeftReverse && !inputRightReverse)
    {
      digitalWrite(mc_I4,HIGH);//turn DC Motor B move clockwise
      digitalWrite(mc_I3,LOW);
      digitalWrite(mc_I2,LOW);//turn DC Motor A move anticlockwise
      digitalWrite(mc_I1,HIGH);
    }
    else if(inputLeftReverse && !inputRightReverse)
    {
      digitalWrite(mc_I4,LOW);//turn DC Motor B move anticlockwise
      digitalWrite(mc_I3,HIGH);
      digitalWrite(mc_I2,LOW);//turn DC Motor A move clockwise
      digitalWrite(mc_I1,HIGH);
    }
    else if(!inputLeftReverse && inputRightReverse)
    {
      digitalWrite(mc_I4,HIGH);//turn DC Motor B move clockwise
      digitalWrite(mc_I3,LOW);
      digitalWrite(mc_I2,HIGH);//turn DC Motor A move clockwise
      digitalWrite(mc_I1,LOW);
    }
    else
    {
      digitalWrite(mc_I4,LOW);//turn DC Motor B move anticlockwise
      digitalWrite(mc_I3,HIGH);
      digitalWrite(mc_I2,HIGH);//turn DC Motor A move clockwise
      digitalWrite(mc_I1,LOW);
    }
    
    delay(msc_switchDelayMillis);
  }
}

MotorPerseus g_motor(LEFT_PIN_POWER, RIGHT_PIN_POWER, PIN_I1, PIN_I2, PIN_I3, PIN_I4);
void setup()
{
  Serial.begin(9600);
  g_motor.init();
  Serial.println("Ready.");
}

int g_lastCommand = ' ';
int g_ticksSinceLastCommand = 0;
void loop()
{
  /*
  int driveLeft = 0;
  int driveRight = 0;
  int inputValue = Serial.read();
  if(inputValue >= 0 && inputValue <= 100)
  {
    Serial.println("INPUT: "+String(inputValue));
    if(inputValue > 40 && inputValue < 60)
    {
      driveLeft = 100;
      driveRight = 100;
    }
    else if(inputValue < 40)
    {
      driveLeft = -100;
      driveRight = 100;
    }
    else
    {
      driveLeft = 100;
      driveRight = -100;
    }

    if(inputValue < 50)
    {
      driveLeft = inputValue*3-100;
      driveRight = 100-inputValue;
    }
    else
    {
      driveLeft = inputValue;
      driveRight = 50-(inputValue-50)*3;
    }
       
    Serial.println("OUTPUT: "+String(inputValue));
    g_motor.drive(driveLeft, driveRight);
  }
  else if(inputValue == 255)
  {
    g_motor.drive(0, 0);
    
  }
  */

  int inputCharacter = Serial.read();
  int effectiveCharacter = inputCharacter;
  if(inputCharacter == -1 && g_ticksSinceLastCommand < 100000)
  {
    ++g_ticksSinceLastCommand;
    effectiveCharacter = g_lastCommand;
  }
  else
  {
    g_ticksSinceLastCommand = 0;
    g_lastCommand = effectiveCharacter;
#ifdef MESSAGE_INFO
    Serial.println(String("Got: ")+effectiveCharacter);
#endif
  }

  switch(effectiveCharacter)
  {
    case 'w':
      g_motor.drive(SPEED_CURRENT, SPEED_CURRENT);
      break;
    case 'a':
      g_motor.drive(-SPEED_CURRENT/3, SPEED_CURRENT/3);
      break;
    case 's':
      g_motor.drive(-SPEED_CURRENT/3, -SPEED_CURRENT/3);
      break;
    case 'd':
      g_motor.drive(SPEED_CURRENT, -SPEED_CURRENT);
      break;
    case ' ':
      g_motor.drive(0, 0);
      break;
    case '0':
      g_motor.drive(100, 0);
      break;
    case '1':
      g_motor.drive(10, 0);
      break;
    case '2':
      g_motor.drive(20, 0);
      break;
    case '3':
      g_motor.drive(30, 0);
      break;
    case '4':
      g_motor.drive(40, 0);
      break;
    case '5':
      g_motor.drive(50, 0);
      break;
    case '6':
      g_motor.drive(60, 0);
      break;
    case '7':
      g_motor.drive(70, 0);
      break;
    case '8':
      g_motor.drive(80, 0);
      break;
    case '9':
      g_motor.drive(90, 0);
      break;
    case ')':
      g_motor.drive(0, 100);
      break;
    case '!':
      g_motor.drive(0, 10);
      break;
    case '@':
      g_motor.drive(0, 20);
      break;
    case '#':
      g_motor.drive(0, 30);
      break;
    case '$':
      g_motor.drive(0, 40);
      break;
    case '%':
      g_motor.drive(0, 50);
      break;
    case '^':
      g_motor.drive(0, 60);
      break;
    case '&':
      g_motor.drive(0, 70);
      break;
    case '*':
      g_motor.drive(0, 80);
      break;
    case '(':
      g_motor.drive(0, 90);
      break;
    default:
#ifdef MESSAGE_INFO
      Serial.println("DEFAULT");
#endif
      g_motor.drive(0, 0);
      break;
  }
}

/*
void setup()
{
  pinMode(pinI1,OUTPUT);
  pinMode(pinI2,OUTPUT);
  pinMode(speedpinA,OUTPUT);
  pinMode(pinI3,OUTPUT);
  pinMode(pinI4,OUTPUT);
  pinMode(speedpinB,OUTPUT);
  digitalWrite(speedpinA,LOW);// Unenble the pin, to stop the motor. this should be done to avid damaging the motor. 
  digitalWrite(speedpinB,LOW);

  pinMode (ENCODER_L,INPUT);
  pinMode (ENCODER_R,INPUT);

  Serial.begin (9600);
  
  Serial.println("Preparing...");
  delay(1000);
  
  attachInterrupt(1, ENCODER_PULSE_L, RISING); // interrupt #1 is on pin 2, watch left encoder for rising pulse
  attachInterrupt(2, ENCODER_PUSLE_R, RISING); // interrupt #2 is on pin 7, watch right encoder for rising pulse   
  Serial.println("Ready.");
}
 
void forward()
{
     analogWrite(speedpinA,speed);//input a simulation value to set the speed
     analogWrite(speedpinB,speed);
     digitalWrite(pinI4,HIGH);//turn DC Motor B move clockwise
     digitalWrite(pinI3,LOW);
     digitalWrite(pinI2,LOW);//turn DC Motor A move anticlockwise
     digitalWrite(pinI1,HIGH);
}
void backward()//
{
     analogWrite(speedpinA,speed);//input a simulation value to set the speed
     analogWrite(speedpinB,speed);
     digitalWrite(pinI4,LOW);//turn DC Motor B move anticlockwise
     digitalWrite(pinI3,HIGH);
     digitalWrite(pinI2,HIGH);//turn DC Motor A move clockwise
     digitalWrite(pinI1,LOW);
}

void right()
{
     analogWrite(speedpinA,speed);//input a simulation value to set the speed
     analogWrite(speedpinB,speed);
     digitalWrite(pinI4,HIGH);//turn DC Motor B move clockwise
     digitalWrite(pinI3,LOW);
     digitalWrite(pinI2,HIGH);//turn DC Motor A move clockwise
     digitalWrite(pinI1,LOW);
}

void left()
{
     analogWrite(speedpinA,speed);//input a simulation value to set the speed
     analogWrite(speedpinB,speed);
     digitalWrite(pinI4,LOW);//turn DC Motor B move anticlockwise
     digitalWrite(pinI3,HIGH);
     digitalWrite(pinI2,LOW);//turn DC Motor A move clockwise
     digitalWrite(pinI1,HIGH);
}

void stop()
{
     digitalWrite(speedpinA,LOW);// Unenble the pin, to stop the motor. this should be done to avid damaging the motor. 
     digitalWrite(speedpinB,LOW);
     delay(100);
 
}

int g_lastCommand = ' ';
int g_ticksSinceLastCommand = 0;
void loop()
{
  int inputCharacter = Serial.read();
  int effectiveCharacter = inputCharacter;
  if(inputCharacter == -1 && g_ticksSinceLastCommand < 100000)
  {
    ++g_ticksSinceLastCommand;
    effectiveCharacter = g_lastCommand;
  }
  else
  {
    g_ticksSinceLastCommand = 0;
    g_lastCommand = effectiveCharacter;
#ifdef MESSAGE_INFO
    Serial.println(String("Got: ")+effectiveCharacter);
#endif
  }
  
    switch(effectiveCharacter)
    {
      case 'a' :
          left();
          break;
       case 'd' :
          right();
          break;
       case 'w' :
          forward();
          break;
       case 's' :
          backward();
          break;
       case '\r':
       case '\n':
         break;
       default :
          stop();
          break;
     }
    //delay(700);
  }
}
*/

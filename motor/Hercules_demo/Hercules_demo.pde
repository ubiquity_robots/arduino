//#include <plib.h>
//#define MESSAGE_INFO

#define real float

static int const LEFT_PIN_PWM = 5;
static int const LEFT_PIN_REVERSE = 3;
static int const RIGHT_PIN_PWM = 6;
static int const RIGHT_PIN_REVERSE = 4;
static real const LEFT_SCALE_FACTOR = 1;
static real const RIGHT_SCALE_FACTOR = 1;
static real USER_SCALE_FACTOR = 1;

enum Result
{
  R_TRUE=1,
  R_FALSE=0,
  R_SUCCESS=0,
  R_FAILURE=-1,
  R_INPUTBAD=-2,
  R_NEEDSINIT=-3
};

real cap(real const i_value, real const i_min, real const i_max)
{
  if(i_value < i_min)
    return i_min;
  else if(i_value > i_max)
    return i_max;
    
  return i_value;
}

void servoWrite(uint8_t const i_pin, uint8_t const i_magnitude255)
{
/*
  static unsigned int servoActiveBitArray = 0;
  uint16_t timer = digitalPinToTimerOC(i_pin);
  if(timer == NOT_ON_TIMER || i_magnitude255 == 0 || i_magnitude255 == 255)
  {
    if(i_magnitude255 < 128)
    {
      digitalWrite(i_pin, LOW);
    }
    else
    {
      digitalWrite(i_pin, HIGH);
    }
  }
  else
  {
    int magnitudeFinal = cap(i_magnitude255, 0, 250);
    
    if(servoActiveBitArray == 0)
    {
      T2CONCLR = T2_ON;
      T2CON = T2_PS_1_8;
      TMR2 = 0;
      PR2 = 1000;
      T2CONSET = T2_ON;
    }
    
    OC1R = magnitudeFinal*4;
    OC1RS = magnitudeFinal*4;
    OC1CON = OC_TIMER2_SRC | OC_PWM_FAULT_PIN_DISABLE;
    OC1CONSET = OC_ON;
  }*/
  //Serial.println(String("W: ") + String((int)i_pin) + String("-") + String((int)i_magnitude255));
  analogWrite(i_pin, i_magnitude255);
}

class MotorHercules
{
public:
  MotorHercules(int const i_leftPinPower, int const i_leftPinReverse, int const i_rightPinPower, int const i_rightPinReverse);
  ~MotorHercules();
  Result init();
  Result drive(int const i_speedLeft, int const i_speedRight);
  
  real speedLeft() {
    return m_speedLeftCurrent;
  }
  real speedRight() {
    return m_speedRightCurrent;
  }
  
  private:
    int const mc_leftPinPower;
    int const mc_leftPinReverse;
    int const mc_rightPinPower;
    int const mc_rightPinReverse;
    
    bool m_setup;
    
    real m_speedLeftCurrent;
    real m_speedLeftDesired;
    real m_speedRightCurrent;
    real m_speedRightDesired;

    static int const msc_switchDelayMillis = 250;
};

MotorHercules::MotorHercules(int const i_leftPinPower, int const i_leftPinReverse, int const i_rightPinPower, int const i_rightPinReverse)
:m_setup(false), m_speedLeftCurrent(0), m_speedLeftDesired(0), m_speedRightCurrent(0), m_speedRightDesired(0), mc_leftPinPower(i_leftPinPower), mc_leftPinReverse(i_leftPinReverse), mc_rightPinPower(i_rightPinPower), mc_rightPinReverse(i_rightPinReverse)
{
}

MotorHercules::~MotorHercules()
{
  if(m_setup)
  {
    digitalWrite(mc_leftPinPower, LOW);
    digitalWrite(mc_rightPinPower, LOW);
    
    delay(msc_switchDelayMillis);
    digitalWrite(mc_leftPinReverse, LOW);
    digitalWrite(mc_rightPinReverse, LOW);
    delay(msc_switchDelayMillis);
  }
}
  
Result MotorHercules::init()
{
  pinMode(mc_leftPinPower, OUTPUT);
  servoWrite(mc_leftPinPower, 0);
  pinMode(mc_rightPinPower, OUTPUT);
  servoWrite(mc_rightPinPower, 0);
  
  delay(msc_switchDelayMillis);
  pinMode(mc_leftPinReverse, OUTPUT);
  digitalWrite(mc_leftPinReverse, LOW);
  pinMode(mc_rightPinReverse, OUTPUT);
  digitalWrite(mc_rightPinReverse, LOW);
  delay(msc_switchDelayMillis);
  
  m_setup = true;
  
  return R_SUCCESS;
}

Result MotorHercules::drive(int const i_speedLeft, int const i_speedRight)
{
  if(!m_setup)
  {
    return R_NEEDSINIT;
  }
  
  real const powerScaleLeft = LEFT_SCALE_FACTOR*USER_SCALE_FACTOR;
  real const powerScaleRight = RIGHT_SCALE_FACTOR*USER_SCALE_FACTOR;
  real inputSpeedLeft=i_speedLeft*powerScaleLeft;
  real inputSpeedRight=i_speedRight*powerScaleRight;
  
  bool inputLeftReverse = inputSpeedLeft < 0;
  bool inputRightReverse = inputSpeedRight < 0;
  
  if(    ((inputSpeedLeft < 0) != (m_speedLeftCurrent < 0))
      || ((inputSpeedRight < 0) != (m_speedRightCurrent < 0)))
  {
    while(m_speedLeftCurrent != 0 || m_speedRightCurrent != 0)
    {
      real speedLeftNew = m_speedLeftCurrent - cap(m_speedLeftCurrent, -powerScaleLeft, powerScaleLeft);
      real speedRightNew = m_speedRightCurrent - cap(m_speedRightCurrent, -powerScaleRight, powerScaleRight);
      
      servoWrite(mc_leftPinPower, cap(abs(speedLeftNew), 0, 100));
      m_speedLeftCurrent = speedLeftNew;
      
      servoWrite(mc_rightPinPower, cap(abs(speedRightNew), 0, 100));
      m_speedRightCurrent = speedRightNew;
      
#ifdef MESSAGE_INFO
      Serial.println(String("L: ")+int(m_speedLeftCurrent)+String("     R:")+int(m_speedRightCurrent));
#endif
      
      delay(10);
    }
    
    delay(msc_switchDelayMillis);
    
    digitalWrite(mc_leftPinReverse, inputLeftReverse);
    digitalWrite(mc_rightPinReverse, inputRightReverse);
    
    delay(msc_switchDelayMillis);
  }
  
  m_speedLeftDesired = cap(inputSpeedLeft, -100, 100);
  m_speedRightDesired = cap(inputSpeedRight, -100, 100);
  
  while((m_speedLeftCurrent != m_speedLeftDesired) || (m_speedRightCurrent != m_speedRightDesired))
  {
    real speedLeftNew = m_speedLeftCurrent + cap(m_speedLeftDesired - m_speedLeftCurrent, -powerScaleLeft, powerScaleLeft);
    real speedRightNew = m_speedRightCurrent + cap(m_speedRightDesired - m_speedRightCurrent, -powerScaleRight, powerScaleRight);
    
    servoWrite(mc_leftPinPower, cap(abs(speedLeftNew), 0, 100));
    m_speedLeftCurrent = speedLeftNew;
    
    servoWrite(mc_rightPinPower, cap(abs(speedRightNew), 0, 100));
    m_speedRightCurrent = speedRightNew;

#ifdef MESSAGE_INFO   
      Serial.println(String("L: ")+int(m_speedLeftCurrent)+String("     R:")+int(m_speedRightCurrent));
#endif
    
    delay(10);
  }
}

MotorHercules g_motor(LEFT_PIN_PWM, LEFT_PIN_REVERSE, RIGHT_PIN_PWM, RIGHT_PIN_REVERSE);
void setup()
{
  Serial.begin(9600);
  g_motor.init();
  Serial.println("Ready.");
}

int g_lastCommand = ' ';
int g_ticksSinceLastCommand = 0;
int const gsc_ticksSinceLastCommandMax = 100000;
void loop()
{
  int inputCharacter = Serial.read();
  int effectiveCharacter = inputCharacter;
  if(inputCharacter == -1 && g_ticksSinceLastCommand < gsc_ticksSinceLastCommandMax)
  {
    ++g_ticksSinceLastCommand;
    effectiveCharacter = g_lastCommand;
  }
  else
  {
    g_ticksSinceLastCommand = 0;
    g_lastCommand = effectiveCharacter;
#ifdef MESSAGE_INFO
    Serial.println(String("Got: ")+effectiveCharacter);
#endif
  }

  switch(effectiveCharacter)
  
  {
    case 'G':
      {
        char rString[4];
        char tString[4];
        delay(10);
        rString[0] = ((char)Serial.read());
        rString[1] = ((char)Serial.read());
        rString[2] = ((char)Serial.read());
        rString[3] = '\0';
        tString[0] = ((char)Serial.read());
        tString[1] = ((char)Serial.read());
        tString[2] = ((char)Serial.read());
        tString[3] = '\0';

        double r=atol(rString);
        double t=atol(tString);
        
        int left=0;
        int right=0;
        
        if(t>0 && t<180)
        {
          left=(r/10.0);
          right=(r/10.0)*((90.0-t)/90.0);
        } else if(t>180.0) {
          left=(r/10.0)*((90.0-(360.0-t))/90.0);
          right=(r/10.0);
        } else {
          left=(r/10.0);
          right=(r/10.0);
        }
        g_motor.drive(left, right);
        delay(300);
        g_ticksSinceLastCommand = gsc_ticksSinceLastCommandMax;
        break;
      }
    case 'i':
      g_motor.drive(25, 25);
      break;
    case 'j':
      g_motor.drive(0, 5);
      break;
    case 'k':
      g_motor.drive(5, 0);
      break;
    case 'w':
      g_motor.drive(50, 50);
      break;
    case 'a':
      g_motor.drive(0, 25);
      break;
    case 's':
      g_motor.drive(-50, -50);
      break;
    case 'd':
      g_motor.drive(25, 0);
      break;
    case 'z':
      g_motor.drive(-25, 0);
      break;
    case 'c':
      g_motor.drive(0, -25);
      break;
    case ' ':
      g_motor.drive(0, 0);
      break;
    case '0':
      g_motor.drive(100, 0);
      break;
    case '1':
      g_motor.drive(10, 0);
      break;
    case '2':
      g_motor.drive(20, 0);
      break;
    case '3':
      g_motor.drive(30, 0);
      break;
    case '4':
      g_motor.drive(40, 0);
      break;
    case '5':
      g_motor.drive(50, 0);
      break;
    case '6':
      g_motor.drive(60, 0);
      break;
    case '7':
      g_motor.drive(70, 0);
      break;
    case '8':
      g_motor.drive(80, 0);
      break;
    case '9':
      g_motor.drive(90, 0);
      break;
    case ')':
      g_motor.drive(0, 100);
      break;
    case '!':
      g_motor.drive(0, 10);
      break;
    case '@':
      g_motor.drive(0, 20);
      break;
    case '#':
      g_motor.drive(0, 30);
      break;
    case '$':
      g_motor.drive(0, 40);
      break;
    case '%':
      g_motor.drive(0, 50);
      break;
    case '^':
      g_motor.drive(0, 60);
      break;
    case '&':
      g_motor.drive(0, 70);
      break;
    case '*':
      g_motor.drive(0, 80);
      break;
    case '(':
      g_motor.drive(0, 90);
      break;
    case '+':
      USER_SCALE_FACTOR = cap (USER_SCALE_FACTOR+0.1, 0.5, 2.0);
#ifdef MESSAGE_INFO
      Serial.println(String("USERS SCALE FACTOR: ") + int(USER_SCALE_FACTOR) + String(".") + (int(USER_SCALE_FACTOR*10)%10));
#endif
      g_ticksSinceLastCommand = gsc_ticksSinceLastCommandMax;
      break;
    case '-':
      USER_SCALE_FACTOR = cap (USER_SCALE_FACTOR-0.1, 0.5, 2.0);
#ifdef MESSAGE_INFO
      Serial.println(String("USERS SCALE FACTOR: ") + int(USER_SCALE_FACTOR) + String(".") + (int(USER_SCALE_FACTOR*10)%10));
#endif
      g_ticksSinceLastCommand = gsc_ticksSinceLastCommandMax;
      break;
    default:
#ifdef MESSAGE_INFO
      //Serial.println("DEFAULT");
#endif
      g_motor.drive(0, 0);
      break;
  }
}

static unsigned int gs_encoderLeftTickCount = 0;
static unsigned int gs_encoderRightTickCount = 0;
static int const INTERRUPT_1_PIN = 2;
static int const INTERRUPT_2_PIN = 7;

static int const gsc_motorRightPinCurrent0=8;//define I1 interface
static int const gsc_motorRightPinCurrent1=11;//define I2 interface 
static int const gsc_motorRightPinSpeed=9;//enable motor A
static int const gsc_motorLeftPinCurrent0=12;//define I3 interface 
static int const gsc_motorLeftPinCurrent1=13;//define I4 interface 
static int const gsc_motorLeftPinSpeed=10;//enable motor B
static int const gsc_motorSpeedHalf = 127;//define the speed of motor

void interruptEncoderLeft()
{
  gs_encoderLeftTickCount += 1;
  Serial.println(String("L: ") + gs_encoderLeftTickCount);
}

void interruptEncoderRight()
{
  gs_encoderRightTickCount += 1;
  Serial.println(String("R: ") + gs_encoderRightTickCount);
}

void setup()
{
  /***** Set motor pins for output *****/
  pinMode(gsc_motorRightPinCurrent0,OUTPUT);
  pinMode(gsc_motorRightPinCurrent1,OUTPUT);
  pinMode(gsc_motorRightPinSpeed,OUTPUT);
  pinMode(gsc_motorLeftPinCurrent0,OUTPUT);
  pinMode(gsc_motorLeftPinCurrent1,OUTPUT);
  pinMode(gsc_motorLeftPinSpeed,OUTPUT);
  
  /***** Stop motor to avoid damaging motor *****/
  digitalWrite(gsc_motorRightPinSpeed,LOW);
  digitalWrite(gsc_motorLeftPinSpeed,LOW);
  
  /***** Setup encoder pins for input *****/
  pinMode (INTERRUPT_1_PIN,INPUT);
  pinMode (INTERRUPT_2_PIN,INPUT);
  Serial.begin (9600);
  attachInterrupt(1, interruptEncoderLeft, RISING);
  attachInterrupt(2, interruptEncoderRight, RISING);
  
  /***** Enable serial interface *****/
  delay(100);
  Serial.println("Ready.");
}

void driveLeft()
{
     analogWrite(gsc_motorLeftPinSpeed,gsc_motorSpeedHalf);
     digitalWrite(gsc_motorLeftPinCurrent1,LOW);//turn DC Motor B move anticlockwise
     digitalWrite(gsc_motorLeftPinCurrent0,HIGH);
     analogWrite(gsc_motorRightPinSpeed,0);//input a simulation value to set the speed
}

void driveRight()
{
     analogWrite(gsc_motorLeftPinSpeed,0);
     analogWrite(gsc_motorRightPinSpeed,gsc_motorSpeedHalf);//input a simulation value to set the speed
     digitalWrite(gsc_motorRightPinCurrent1,HIGH);//turn DC Motor A move clockwise
     digitalWrite(gsc_motorRightPinCurrent0,LOW);
}

void stop()
{
     digitalWrite(gsc_motorRightPinSpeed,LOW);// Unenble the pin, to stop the motor. this should be done to avid damaging the motor. 
     digitalWrite(gsc_motorLeftPinSpeed,LOW);
     delay(100);
}

void loop()
{
  int incomingByte=Serial.read();
  switch(incomingByte)
  {
    case -1:
      break;
    case 'l':
      driveLeft();
      break;
    case 'r':
      driveRight();
      break;
    default:
      stop();
      break;
  }
}

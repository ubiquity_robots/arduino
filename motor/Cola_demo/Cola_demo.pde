

#define TIMER_PRD CORE_TICK_RATE * 100
#define RELAY_SWITCH_TIME CORE_TICK_RATE * 250
#define MAX_SPEED_DELT 5
#define RAMP_DOWN_DIST 100
#define SERIAL_TIMEOUT 0x10000000l

#define SUCCESS 0
#define FAILURE 1
#define FORWARD 0
#define BACKWARD 1

#define SLOW 40
#define FAST 80

static int const LEFT_PIN_PWM = 5;
static int const LEFT_PIN_REVERSE = 3;
static int const RIGHT_PIN_PWM = 6;
static int const RIGHT_PIN_REVERSE = 4;

enum Maneuver_Type
{
  M_IDLE = 0,
  M_MOVE = 1,
  M_TURN = 2
};

struct Maneuver
{
  Maneuver_Type type;
  int l_target, r_target;
  int l_current, r_current;
  int l_target_speed, r_target_speed;
  int l_current_speed, r_current_speed;
  bool l_dir, r_dir;
};

Maneuver current_maneuver;
volatile bool new_maneuver = false;
volatile int currentSpeed = SLOW;

#define MIN(a, b) (((a) < (b))? (a) : (b))
#define MAX(a, b) (((a) > (b))? (a) : (b))

float cap(float const i_value, float const i_min, float const i_max)
{
  if(i_value < i_min)
    return i_min;
  else if(i_value > i_max)
    return i_max;
    
  return i_value;
}

void execute_turn(int angle) {
  //Do some magic to determine how much each wheel needs to turn
  angle = angle % 360;
  bool right_turn;
  if(angle > 180) { //Turn left
    angle = 360 - angle;
    right_turn = false;
  } else { //Turn right
    right_turn = true;
  }
  
  int dist = (M_2_PI / 360.0f) * 12 * angle;
  if(right_turn) {
    current_maneuver.l_target = dist;
    current_maneuver.l_target_speed = currentSpeed;
    current_maneuver.r_target = 0;
    current_maneuver.r_target_speed = 0;
  } else {
    current_maneuver.l_target = 0;
    current_maneuver.l_target_speed = 0;
    current_maneuver.r_target = dist;
    current_maneuver.r_target_speed = currentSpeed;
  }
  current_maneuver.type = M_TURN;
  new_maneuver = true;
  
  //Block until the turn is done
  while(current_maneuver.type == M_TURN);
}

void execute_move(float dist) {
  //Do some magic to determine how much each wheel needs to turn
  int ticks = dist * 4; // dist is in cm
  current_maneuver.type = M_MOVE;
  current_maneuver.l_target = ticks;
  current_maneuver.r_target = ticks;
  current_maneuver.l_target_speed = currentSpeed;
  current_maneuver.r_target_speed = currentSpeed;
  new_maneuver = true;
}

void go(const char* cmd) {
  int dist = 0;
  int theta = 0;
  
  scanf(" %d %d", dist, theta);
  execute_turn(theta);
  execute_move(dist);
}

void execute_stop() {
  current_maneuver.type = M_IDLE;
  kill_motors();
  new_maneuver = true;
}

///////////////// Encoder communication functions /////////////////
int reset_encoders() {

  return SUCCESS;  
}

int get_encoders(int *l_current, int *r_current)
{
  
  return SUCCESS;
}

//Makes the motors stop spinning.
void kill_motors() {
  servoWrite(LEFT_PIN_PWM, 0);
  servoWrite(RIGHT_PIN_PWM, 0);
}

void servoWrite(uint8_t const i_pin, uint8_t const i_magnitude255) {
  analogWrite(i_pin, i_magnitude255);
}

//Async timer callback. Executes every 100 ms to update what the motors should be doing
uint32_t update_motors(uint32_t currentTime) {
  if(new_maneuver) {
    current_maneuver.l_current = 0;
    current_maneuver.r_current = 0;
    reset_encoders();
    new_maneuver = false;
    //Do we need to switch the direction relays?
    if((current_maneuver.l_target < current_maneuver.l_current) != current_maneuver.l_dir ||
       (current_maneuver.r_target < current_maneuver.r_current) != current_maneuver.r_dir) {
      kill_motors();
      current_maneuver.l_dir = (current_maneuver.l_target < current_maneuver.l_current);
      current_maneuver.r_dir = (current_maneuver.r_target < current_maneuver.r_current);
      digitalWrite(LEFT_PIN_REVERSE, current_maneuver.l_dir);
      digitalWrite(RIGHT_PIN_REVERSE, current_maneuver.r_dir);
      return currentTime + RELAY_SWITCH_TIME; //Yield for enough time to switch the relays
    }
  }
  
  if(current_maneuver.type == M_IDLE) {
    kill_motors();
    return currentTime + TIMER_PRD;
  }
  
  //Update how far each motor has to go
  get_encoders(&current_maneuver.l_current, &current_maneuver.r_current);
  int l_dist = current_maneuver.l_target - current_maneuver.l_current;
  int r_dist = current_maneuver.r_target - current_maneuver.r_current;
  
  //Handle overshoot case
  if(abs(current_maneuver.l_current) > abs(current_maneuver.l_target)) {
    //servoWrite(LEFT_PIN_PWM, 0);
    current_maneuver.l_target_speed = 0;
    l_dist = 0;
  }
  if(abs(current_maneuver.r_current) > abs(current_maneuver.r_target)) {
    //servoWrite(RIGHT_PIN_PWM, 0);
    current_maneuver.r_target_speed = 0;
    r_dist = 0;
  }
  if(l_dist == 0 && r_dist == 0) { //Maneuver complete
    current_maneuver.type = M_IDLE;
    kill_motors();
    return currentTime + TIMER_PRD;
  } else { //Do the move
    current_maneuver.l_current_speed = min(current_maneuver.l_current_speed + MAX_SPEED_DELT, current_maneuver.l_target_speed);
    current_maneuver.r_current_speed = min(current_maneuver.r_current_speed + MAX_SPEED_DELT, current_maneuver.r_target_speed);
  }
  servoWrite(LEFT_PIN_PWM, current_maneuver.l_current_speed);
  servoWrite(RIGHT_PIN_PWM, current_maneuver.r_current_speed);
  return currentTime + TIMER_PRD;
}

int motor_init()
{
  pinMode(LEFT_PIN_PWM, OUTPUT);
  servoWrite(LEFT_PIN_PWM, 0);
  pinMode(RIGHT_PIN_PWM, OUTPUT);
  servoWrite(RIGHT_PIN_PWM, 0);
  
  delay(RELAY_SWITCH_TIME);
  pinMode(LEFT_PIN_REVERSE, OUTPUT);
  digitalWrite(LEFT_PIN_REVERSE, LOW);
  pinMode(RIGHT_PIN_REVERSE, OUTPUT);
  digitalWrite(RIGHT_PIN_REVERSE, LOW);
  delay(RELAY_SWITCH_TIME);
  
  return SUCCESS;
}

void setup() {
  Serial.begin(115200);
  motor_init();
  //attachCoreTimerService(update_motors);
  Serial.println("Ready.");
}

//Reads len chars into a buffer, or fails when timeout pollcount happens with no data
int read_buf(char *buf, uint32_t len, uint64_t timeout)
{
  uint32_t readLen = 0;
  uint32_t idleTime = 0;
  bool done = false;
  while(!done) {
    while(Serial.available() == 0) {
      idleTime++;
      if(idleTime > timeout)
        return FAILURE;
    }
    idleTime = 0;
    buf[readLen++] = Serial.read();
    if(readLen == len)
      done = true;
  }
  return SUCCESS;
}


void loop() {
  char command_buffer [100];
  int status = SUCCESS;
  //Read a command
  uint64_t idleTime = 0;
  while(Serial.available() == 0) {
    if(++idleTime > SERIAL_TIMEOUT) {
      execute_stop();
      return;
    }
  }
  
  char cmd = Serial.read();
  switch(cmd) {
    case 'G' : //Go
      status = read_buf(command_buffer, 8, SERIAL_TIMEOUT);
      if(status == SUCCESS) {
        command_buffer[8] = 0;
      } else {
        execute_stop();
      }
      break;
    case 'S' : //Status
      break;
    case 'V' : //Speed
      status = read_buf(command_buffer, 5, SERIAL_TIMEOUT);
      if(status == SUCCESS) {
        command_buffer[5] = 0;
      } else {
        execute_stop();
      }
      break;
    case 'U' : //Sensor
      //Implement me!
      break;
    default:
      break;
  }

}


//. Motor driver shield- 2012 Copyright (c) Seeed Technology Inc.
// 


int pinI1=8;//define I1 interface
int pinI2=11;//define I2 interface 
int speedpinA=9;//enable motor A
int pinI3=12;//define I3 interface 
int pinI4=13;//define I4 interface 
int speedpinB=10;//enable motor B
int speed = 127;//define the speed of motor

//Uno32 External Interrupt pins:  ** INT0 = 38, INT1 = 2, INT2 = 7, INT3 = 8, INT4 = 35
int ENCODER_L = 2;  // pin #
int ENCODER_R = 7;  // pin #
int encoder0Pos = 0;
int encoder1Pos = 0;


// ENCODERS //
void ENCODER_PULSE_L(){
encoder0Pos++;}

void ENCODER_PUSLE_R(){
encoder1Pos++;}

void setup()
{
  pinMode(pinI1,OUTPUT);
  pinMode(pinI2,OUTPUT);
  pinMode(speedpinA,OUTPUT);
  pinMode(pinI3,OUTPUT);
  pinMode(pinI4,OUTPUT);
  pinMode(speedpinB,OUTPUT);
  digitalWrite(speedpinA,LOW);// Unenble the pin, to stop the motor. this should be done to avid damaging the motor. 
  digitalWrite(speedpinB,LOW);

  pinMode (ENCODER_L,INPUT);
  pinMode (ENCODER_R,INPUT);

  Serial.begin (9600);
  Serial.println("Ready. Type character to go: f,b,l,r or digit");
  Serial.println("Type d for 10 sec delay");
 // delay(25000);  //time to unplug the link with computer
  
  attachInterrupt(1, ENCODER_PULSE_L, RISING); // interrupt #1 is on pin 2, watch left encoder for rising pulse
  attachInterrupt(2, ENCODER_PUSLE_R, RISING); // interrupt #2 is on pin 7, watch right encoder for rising pulse   
  delay(100);
 
}
 
void forward()
{
     analogWrite(speedpinA,speed);//input a simulation value to set the speed
     analogWrite(speedpinB,speed);
     digitalWrite(pinI4,HIGH);//turn DC Motor B move clockwise
     digitalWrite(pinI3,LOW);
     digitalWrite(pinI2,LOW);//turn DC Motor A move anticlockwise
     digitalWrite(pinI1,HIGH);
}
void backward()//
{
     analogWrite(speedpinA,speed);//input a simulation value to set the speed
     analogWrite(speedpinB,speed);
     digitalWrite(pinI4,LOW);//turn DC Motor B move anticlockwise
     digitalWrite(pinI3,HIGH);
     digitalWrite(pinI2,HIGH);//turn DC Motor A move clockwise
     digitalWrite(pinI1,LOW);
}
void right()
{
     analogWrite(speedpinA,speed);//input a simulation value to set the speed
     analogWrite(speedpinB,speed);
     digitalWrite(pinI4,HIGH);//turn DC Motor B move clockwise
     digitalWrite(pinI3,LOW);
     digitalWrite(pinI2,HIGH);//turn DC Motor A move clockwise
     digitalWrite(pinI1,LOW);
}
void left()
{
     analogWrite(speedpinA,speed);//input a simulation value to set the speed
     analogWrite(speedpinB,speed);
     digitalWrite(pinI4,LOW);//turn DC Motor B move anticlockwise
     digitalWrite(pinI3,HIGH);
     digitalWrite(pinI2,LOW);//turn DC Motor A move clockwise
     digitalWrite(pinI1,HIGH);
}
void stop()
{
     digitalWrite(speedpinA,LOW);// Unenble the pin, to stop the motor. this should be done to avid damaging the motor. 
     digitalWrite(speedpinB,LOW);
     delay(100);
 
}

void loop()
{
  int pos0, dist0; 
  int pos1, dist1; 
  int incomingByte;
 

  if(Serial.available() <= 0)
    stop(); 
  else
  {
    int stepCount=1;  // one step default
    pos0=encoder0Pos;
    pos1=encoder1Pos;
    
    switch(incomingByte=Serial.read())
    {    
      case '1' ... '9' :   
          forward();
          stepCount=incomingByte-'0';
          break;
      case 'l' :
          left();
          break;
       case 'r' :
          right();
          break;
       case 'f' :
          forward();
          break;
       case 'b' :
          backward(); 
          break;         
       case 'd' :
          stepCount=0;
          delay(10000);
          break;                   
       default :
          stepCount=0;
          stop();
          break;
    }

   int ticks=800 * stepCount; 
// wheel diameter 2.5", circumference 7.85" = 20 cm, 800 tics per revolution 
// 90 deg turn = 1 wheel revoluton = 800 tics

    if(stepCount>0)
      do { 
        delay(5);
        dist0=encoder0Pos-pos0;
        dist1=encoder1Pos-pos1;
      }
      while((dist0 < ticks) && (dist1 < ticks));
    Serial.print(dist0);   Serial.print(' ');   Serial.println(dist1);
  }
  /*
  left();
  Serial.print(encoder0Pos);   Serial.print(' ');   Serial.println(encoder1Pos);  
    delay(2000);
  stop();
  right();
  Serial.print(encoder0Pos);   Serial.print(' ');   Serial.println(encoder1Pos);
  delay(2000);
  stop();
  //delay(2000);
  forward();
  Serial.print(encoder0Pos);   Serial.print(' ');   Serial.println(encoder1Pos);
  delay(2000);
  stop();
  backward();
  Serial.print(encoder0Pos);   Serial.print(' ');   Serial.println(encoder1Pos);
  delay(2000); 
  stop(); 
  */
}

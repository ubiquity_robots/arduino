// This code has not been tested
#define M1TL 2
#define M1TR 3
#define M1BR 4
#define M1BL 5

void setup() {
  pinMode(M1TL,OUTPUT);
  pinMode(M1TR,OUTPUT);
  pinMode(M1BL,OUTPUT);
  pinMode(M1BR,OUTPUT);
  delay(10);
  slow_decay();
}

void loop(){
  forward();
  delay(1000);
  backward();
  delay(1000);
  fast_decay();
  delay(1000);
  slow_decay();
  delay(1000);
}

void forward(){
  digitalWrite(M1TL,HIGH);
  digitalWrite(M1TR,LOW);
  digitalWrite(M1BR,HIGH);
  digitalWrite(M1BL,LOW);
}

void backward(){
  digitalWrite(M1TL,LOW);
  digitalWrite(M1TR,HIGH);
  digitalWrite(M1BR,LOW);
  digitalWrite(M1BL,HIGH);
}

void fast_decay(){
  digitalWrite(M1TL,HIGH);
  digitalWrite(M1TR,HIGH);
  digitalWrite(M1BR,LOW);
  digitalWrite(M1BL,LOW);
}

void slow_decay(){
  digitalWrite(M1TL,LOW);
  digitalWrite(M1TR,LOW);
  digitalWrite(M1BR,HIGH);
  digitalWrite(M1BL,HIGH);
}



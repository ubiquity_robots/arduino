#ifndef H_BRIDGE
#define H_BRIDGE

#include "WProgram.h"

class HBridgeHercules
{ 
public:
	HBridgeHercules();
	virtual ~HBridgeHercules();

	void start();
	void drive(int const i_powerLeft255, int const i_powerRight255);

private:
	HBridgeHercules(HBridgeHercules const &)=0;
	HBridgeHercules &operator =(HBridgeHercules const &)=0;

	bool m_started;

	static int const M1TL = 2;
	static int const M1TR = 3;
	static int const M1BR = 28;
	static int const M1BL = 29;

	static int const M2TL = 4;
	static int const M2TR = 5;
	static int const M2BR = 30;
	static int const M2BL = 31;

	static int const POWER_MAX = 255;
};

#endif

#include "HBridgeHercules.h"
#include "WProgram.h"

static int cap(int const i_value, int const i_min, int const i_max)
{
  if(i_value < i_min)
    return i_min;
  else if(i_value > i_max)
    return i_max;

  return i_value;
}

HBridgeHercules::HBridgeHercules()
{
	m_started = false;
}

HBridgeHercules::~HBridgeHercules()
{
	this->stop();
}

void HBridgeHercules::start()
{
   pinMode(M1TL,OUTPUT);
   pinMode(M1TR,OUTPUT);
   pinMode(M1BL,OUTPUT);
   pinMode(M1BR,OUTPUT);
   pinMode(M2TL,OUTPUT);
   pinMode(M2TR,OUTPUT);
   pinMode(M2BL,OUTPUT);
   pinMode(M2BR,OUTPUT);

	digitalWrite(M1TL, LOW);
   digitalWrite(M1TR, LOW);
   digitalWrite(M1BL, LOW);
   digitalWrite(M1BR, LOW);
   digitalWrite(M2TL, LOW);
   digitalWrite(M2TR, LOW);
   digitalWrite(M2BL, LOW);
   digitalWrite(M2BR, LOW);

	m_started = true;
}

void HBridgeHercules::stop()
{
	if(m_started)
	{
		digitalWrite(M1TL, LOW);
		digitalWrite(M1TR, LOW);
		digitalWrite(M1BL, LOW);
		digitalWrite(M1BR, LOW);

		digitalWrite(M2TL, LOW);
		digitalWrite(M2TR, LOW);
		digitalWrite(M2BL, LOW);
		digitalWrite(M2BR, LOW);
	}
}

void HBridgeHercules::drive(int const i_powerLeft255, int const i_powerRight255)
{
	int powerLeft = cap(i_powerLeft255, 0, POWER_MAX);
	int powerRight = cap(i_powerRight255, 0, POWER_MAX);

	if(m_started)
	{
		if(powerLeft>=0)
		{ 
			analogWrite(M1TL, abs(powerLeft));
		   digitalWrite(M1TR, LOW);
		   digitalWrite(M1BL, LOW);
			digitalWrite(M1BR, HIGH);
		}
		else if(powerLeft<0)
		{
			digitalWrite(M1TL, LOW);
         analogWrite(M1TR, abs(powerLeft));
         digitalWrite(M1BL, HIGH);
         digitalWrite(M1BR, LOW);
		}

		if(powerRight>=0)
		{
         digitalWrite(M2TL, LOW);
         analogWrite(M2TR, abs(powerRight));
         digitalWrite(M2BL, HIGH);
         digitalWrite(M2BR, LOW);
      }
		else if(powerRight<0)
		{
         analogWrite(M2TL, abs(powerRight));
         digitalWrite(M2TR, LOW);
         digitalWrite(M2BL, LOW);
         digitalWrite(M2BR, HIGH);
      }
			
	}
}	

/***** Hardcoded values *****/
static int const ENCODER_LEFT_PIN = 2;
static int const ENCODER_RIGHT_PIN = 3;
static int CM_PER_TICK = 4;

struct EncoderData
{
  volatile int m_tickCount;
  volatile int m_tickPeriod;
  volatile unsigned long m_tickPrevious;
  
public:
  EncoderData() {
    this->clear();
  }
  
  void clear() {
    memset(this, 0, sizeof(*this));
  }
};

typedef struct EncoderData EncoderData_t;

static EncoderData_t gs_encoderLeft;
static EncoderData_t gs_encoderRight;

void encoderUpdate(volatile struct EncoderData * const io_encoderData)
{
  unsigned long const currentTimeElapsed = millis();
  
  /***** RISING *****/
  io_encoderData->m_tickPeriod = currentTimeElapsed-io_encoderData->m_tickPrevious;
  io_encoderData->m_tickPrevious = currentTimeElapsed;
  ++io_encoderData->m_tickCount;
}

void interruptEncoderLeft(void)
{
  encoderUpdate(&gs_encoderLeft);
}

void interruptEncoderRight(void)
{
  encoderUpdate(&gs_encoderRight);
}

void setup() {
  Serial.begin(9600);
  pinMode(ENCODER_LEFT_PIN, INPUT);
  pinMode(ENCODER_RIGHT_PIN, INPUT);

  attachInterrupt(1, interruptEncoderLeft, RISING);
  attachInterrupt(2, interruptEncoderRight, RISING);
}

void loop(){
  //Serial.println(String("L: (") + gs_encoderLeft.m_tickCount + String(" ") + gs_encoderLeft.m_tickPeriod + String(" ") + gs_encoderLeft.m_tickPrevious + String(")"));
  //Serial.println(String("R: (") + gs_encoderRight.m_tickCount + String(" ") + gs_encoderRight.m_tickPeriod + String(" ") + gs_encoderRight.m_tickPrevious + String(")"));
  char outputBuffer[8];
  outputBuffer[0] = 'L';
  sprintf(&outputBuffer[1], "%5i", gs_encoderLeft.m_tickCount*CM_PER_TICK);
  Serial.print(outputBuffer);
  
  outputBuffer[1] = 'R';
  sprintf(outputBuffer, "%5i", gs_encoderLeft.m_tickCount*CM_PER_TICK);
  Serial.print(outputBuffer);
  
  gs_encoderLeft.clear();
  gs_encoderRight.clear();
  delay(500);
} 

void sendPulseToArduino(int pinNumber, long startPulseDelay, long endPulseDelay)
{
  pinMode(pinNumber, OUTPUT);
  digitalWrite(pinNumber, LOW);
  delayMicroseconds(startPulseDelay);
  digitalWrite(pinNumber, HIGH);
  delayMicroseconds(endPulseDelay);
  digitalWrite(pinNumber, LOW);
}

long readPulseFromArduino(int pinNumber)
{
  pinMode(pinNumber, INPUT);
  int duration = pulseIn(pinNumber, HIGH);
  return duration;
}

long hy_srf05_sensor (int startPulsePin, int readPulsePin) // HY- SRF05 Uses two digital pins
{
  sendPulseToArduino(startPulsePin, 2, 5); // Delays in milliseconds
  int duration = readPulseFromArduino(readPulsePin);
  int cm = microsecondsToCentimeters(duration);
  
  if (cm <= 0)
  {
    cm = -1;
  }
  
  return cm;
}

long vexSensor(int startPulsePin, int readPulsePin) // Vex model 276-2155
{
  sendPulseToArduino(startPulsePin, 2, 5); // Delays in milliseconds
  int duration = readPulseFromArduino(readPulsePin);
  int cm = microsecondsToCentimeters(duration);

  /***** Return -1 on error *****/
  if (cm == -499)
  {
    cm = -1;
  }
  
  return cm;
}

long pingSensor(int pinNumber) // Sensor Model Ping ))) and manufaturer www.parallax.com
{
  sendPulseToArduino(pinNumber, 2, 5); // Delays in milliseconds
  int duration = readPulseFromArduino(pinNumber);
  int cm = microsecondsToCentimeters(duration);
  
  /***** Return -1 on blocked sensor *****/
  if (cm > 300)
  {
    cm = -1;
  }
  
  return cm;
}

long microsecondsToInches(long const i_microseconds)
{
  // According to Parallax's datasheet for the PING))), there are
  // 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
  // second). This gives the distance travelled by the ping, outbound
  // and return, so we divide by 2 to get the distance of the obstacle.
  // See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
  
  return long(double(i_microseconds) / 73.746 / 2.0);
}
long microsecondsToCentimeters(long const i_microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return long(double(i_microseconds) / 29.387 / 2.0);
}
